#!/usr/bin/perl

use LWP::Simple;

print "Downloading word list from Project Gutenberg ...\n";

my $resp = getstore('https://www.gutenberg.org/files/3201/files/SINGLE.TXT', 'single.txt');
die "Expected a 200 code, got $resp" unless $resp == 200;


open FIL, 'single.txt' or die $!;
while (! eof FIL) {
	chomp(my $word = <FIL>);
	$word =~ s/\s+$//;
	next if length $word < 3;
	next if $word =~ /[^a-z]/i;

	for (my $n = 0; $n < length $word; ++$n) {
		my $tmp = $word;
		substr($tmp,$n,1) = '.';
		$CACHE{$tmp} ||= [ ];
		push @{$CACHE{$tmp}}, $word;
	}
}
close FIL;

# write out files

for my $wordlength (3 .. 9) {
	my @k = grep { length $_ == $wordlength } keys %CACHE;
	print "writing out wordlist $wordlength\n";
	open FIL, ">words-$wordlength.txt" or die $!;
	foreach my $thisk (@k) {
		my $a = $CACHE{$thisk};
		for (my $i = 0; $i < scalar(@$a) - 1; ++$i) {
			for (my $j = $i + 1; $j < scalar(@$a); ++$j) {
				print FIL $a->[$i] . ',' . $a->[$j] . "\n";
			}
		}
	}
	close FIL;
}

print "done.\n";
__END__

