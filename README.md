# Word Ladder Generator

Grabs a list of words from Project Gutenberg and generates a bunch of word pairs that can be used to generate word ladders.

## How do it?

1. Run the prep script: `perl prep.pl`
2. Run the Dijkstra algorithm script: `perl dijkstra.pl word-one word-two`
3. Interpet the output. (advanced level only)

The arguments to the Dijkstra algorithm script should be two words of the same length. If either word is not within the vocabulary of the script, it will die. 

Also note that you may get no output, if there is no path between the words. You just gotta run the algorithm to confirm the existence of a path.

