#!/usr/bin/perl
use strict;
use Data::Dumper;
use Getopt::Std;

my(%VERTEX,%NEIGHBOR,%parent);
our($opt_f);
getopts('f');

#

if ($ARGV[0] eq '' || $ARGV[1] eq '' || length $ARGV[0] != length $ARGV[1]) {
	print "put two words in the arguments!\n"; exit 0;
}

my $w1 = $ARGV[0];
my $w2 = $ARGV[1];
my $l  = length $w1;

my $fil = sprintf("words-%d.txt", $l);
if (! -e $fil) { print "can only handle words from 3 to 9 letters in length\n"; exit 0; }
#print "building graph\n";
open FIL, $fil or die $!;
while (! eof FIL) {
	chomp(my $line = <FIL>); $line =~ s/\s+$//;
	my(@w) = split(/,/,$line);
	$VERTEX{$w[0]} = 1; $VERTEX{$w[1]} = 1;
	$NEIGHBOR{$w[0]} ||= [] ;
	$NEIGHBOR{$w[1]} ||= [] ;
	push @{$NEIGHBOR{$w[0]}}, $w[1];
	push @{$NEIGHBOR{$w[1]}}, $w[0];
}
close FIL;
#print "done\n";

if (! exists $VERTEX{$w1} || ! exists $VERTEX{$w2}) { warn "those words aren't in my dictionary\n"; exit 0; }


#print Dumper(\%VERTEX); print Dumper(\%NEIGHBOR); exit 0;

sub dijkstra {
	my($start) = @_;
	my(%intree,%distance);
	foreach my $k (keys %VERTEX) {
		$intree{$k} = 0;
		$distance{$k} = 9999;
		$parent{$k} = '';
	}
	
	$distance{$start} = 0;
	my $v = $start;
	my(%VCOPY) = %VERTEX;

	while (! $intree{$v}) {
		$intree{$v} = 1;
		delete $VCOPY{$v};

		foreach my $n (@{$NEIGHBOR{$v}}) {
			if ($distance{$n} > $distance{$v} + 1) {
				$distance{$n} = $distance{$v} + 1;
				$parent{$n} = $v;
			}
		}
		my $dist = 9999;
		$v = '';
		foreach my $i (keys %VCOPY) {
			$v = $i if $v eq '';
			if (! $intree{$i} && $dist > $distance{$i}) {
				$dist = $distance{$i};
				$v = $i;
			}
		}
	}
	#print Dumper(\%intree); print Dumper(\%parent); print Dumper(\%distance); exit 0;
}

#print "traversing graph (start = $w1)\n";
dijkstra($w1);
#print "done.\n";

my $tmp = $w2;
#print "starting with $tmp (parent = $parent{$tmp})\n";

my(@WORDS) = ();
while ($parent{$tmp} ne '') {
	unshift @WORDS, $tmp;
	#print "parent of $tmp is $parent{$tmp}\n";
	$tmp = $parent{$tmp};
}
exit 0 if scalar @WORDS == 0;
unshift @WORDS, $tmp;

if ($opt_f) { open FIL, '>out/'.$WORDS[0].'-'.$WORDS[-1].'.txt' or die $!; print FIL "$_\n" foreach @WORDS; close FIL; } else {
print "$_\n" foreach @WORDS;
}

